package photos.model;

import java.io.IOException;

/**
 * Jean Moscoso
 * Greyson Frazier
 * 
 */


import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 */

public class User implements Serializable {

	private static final long serialVersionUID = 1272880243458262535L;
	private String name;
	public ArrayList<PhotoAlbum> albumList;
	public ArrayList<String> nameList;
	private PhotoAlbum currentAlbum = new PhotoAlbum();
	private PhotoAlbum destinationAlbum = new PhotoAlbum();
	
	/**
	 * Constructor of the user
	 * @param name
	 */
	public User(String name) 
	{
		albumList = new ArrayList<PhotoAlbum>();
		nameList = new ArrayList<String>();
		this.name = name;
	}
	
	/**
	 * @return The name of the user
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * @return ArrayList of the albums within the user.
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public ArrayList<PhotoAlbum> getPhotoAlbum() throws ClassNotFoundException, IOException{
		UserList.getFromDAT();
		
		//System.out.println(UserList.getCurrentUser().albumList.get(0));
		//saveToDAT();
		return albumList;
		
	}
	
	/**
	 * Add an album to the arrayList of users
	 * @param album
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void addAlbum(PhotoAlbum album) throws ClassNotFoundException, IOException {
		//UserList.getFromDAT();
		
		//System.out.println("successful add: " + album.getName());
		//User.albumList.add(album);
		UserList.getCurrentUser().albumList.add(album);
		UserList.getCurrentUser().nameList.add(album.getName());
		/////UserList.getCurrentUser().albumList.add(album);
		UserList.saveToDAT();
		
	}
	
	/**
	 * Sets the current album
	 * @param album
	 */
	public void setCurrentAlbum(PhotoAlbum album) {
		currentAlbum = album;
	}
	
	/**
	 * @return The current album
	 */
	public PhotoAlbum getCurrentAlbum() {
		return currentAlbum;
	}
	
    /**
     * @return Gets the album list as a ArrayList of strings
     */
    public ArrayList<String> getAlbumListAsString()
    {
        UserList.getFromDAT();
        ArrayList<String> list = new ArrayList<>();
        for(PhotoAlbum photoAlbum: albumList)
        {
            list.add(photoAlbum.getName());
        }
        return list;
    }

	/**
	 * @return The destination album
	 */
	public PhotoAlbum getDestinationAlbum() {
		return destinationAlbum;
	}

	/**
	 * Sets the destination album
	 * @param destinationAlbum
	 */
	public void setDestinationAlbum(PhotoAlbum destinationAlbum) {
		this.destinationAlbum = destinationAlbum;
	}

	
}
