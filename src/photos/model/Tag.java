package photos.model;

import java.io.Serializable;

/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 */

public class Tag implements Serializable {
	
	private static final long serialVersionUID = -6619680224678279158L;
	
	private String type;
	private String value;
	
	/**
	 * Constructor of the tag
	 * @param type
	 * @param value
	 */
	public Tag(String type, String value) {
		this.type = type;
		this.value = value;
	}
	
	/**
	 * @return The type of the tag
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type of the tag
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return The value of the tag
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value of the tag
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

}