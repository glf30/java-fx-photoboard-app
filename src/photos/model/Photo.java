package photos.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javafx.scene.image.Image;

/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 */

public class Photo implements Serializable{
	
	private static final long serialVersionUID = 7761755855823637394L;
	private String name;
	private String date;
	private String caption;
	private String photoPath;
	private String album;
	private Image photoImage;
	private String time;
	
	
	private Date calDate;
	
	
	public ArrayList<Tag> tagList;
	
	/**
	 * @param path
	 */
	public Photo(String path){
		//photoImage = image;
		this.name = "";
		photoPath = path;
		tagList = new ArrayList<Tag>();

	}
	
	/**
	 * @param p
	 * @param newAlbum
	 */
	public Photo(Photo p, String newAlbum) {
		this.name = p.name;
		this.date = p.date;
		this.caption = p.caption;
		this.photoPath = p.photoPath;
		this.photoImage = p.photoImage;
		this.tagList = p.tagList;
		this.calDate = p.calDate;
		this.time = p.time;
		this.album = newAlbum;
		
	}
	
	/**
	 * @return The image of the photo
	 */
	public Image getImage() {
		return photoImage;
	}
	
	/**
	 * @return The path of the photo
	 */
	public String getPath() {
		return photoPath;
	}
	
	/**
	 * @return The name of the photo
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Sets the name of the photo
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return The caption of the photo
	 */
	public String getCaption() {
		return this.caption;
	}
	
	/**
	 * Sets the caption of the photo
	 * @param s
	 */
	public void setCaption(String s) {
		this.caption = s;
	}
	
	/**
	 * Sets the date of the photo
	 * @param s
	 */
	public void setDate(String s) {
		this.date = s;
	}
	
	/**
	 * @return The date
	 */
	public String getDate() {
		
		return this.date; 
	}

	/**
	 * Adds a tag to the tag list of the photo
	 * @param type
	 * @param value
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void addTag(String type, String value) throws ClassNotFoundException, IOException {
		//Entry e = new Entry();
    	
    	tagList.add(new Tag(type,value));
		//UserList.getCurrentUser().getCurrentAlbum().photoList.add(photo);
		//UserList.getCurrentUser().getCurrentAlbum().nameList.add(photo.getName());
		//System.out.println("ADDED TO LIST SIZE: " + nameList.size());
		UserList.saveToDAT();
	}

	/**
	 * @returns The album
	 */
	public String getAlbum() {
		return album;
	}

	/**
	 * Sets the current album
	 * @param album
	 */
	public void setAlbum(String album) {
		this.album = album;
	}

	/**
	 * @return The calendar date
	 */
	public Date getCalDate() {
		return calDate;
	}

	/**
	 * @param calDate
	 */
	public void setCalDate(Date calDate) {
		this.calDate = calDate;
	}

	/**
	 * @return The time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param time
	 */
	public void setTime(String time) {
		this.time = time;
	}
	
    
	
	
}