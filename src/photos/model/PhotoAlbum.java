package photos.model;

import java.io.IOException;

/**
 * Jean Moscoso
 * Greyson Frazier
 * 
 */


import java.io.Serializable;
import java.util.ArrayList;

import javafx.scene.image.ImageView;

/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 */

public class PhotoAlbum implements Serializable{

	private static final long serialVersionUID = 4944925890542055499L;
	private String name;
	private ImageView albumImage;
	public ArrayList<Photo> photoList;
	public ArrayList<String> nameList;
	
	private Photo currentPhoto = new Photo("");
	
	/**
	 * Constructor of the photo album
	 */
	public PhotoAlbum () {
		photoList = new ArrayList<Photo>();
		nameList = new ArrayList<String>();		
	}
	
	/**
	 * Constructor of the photo album
	 * @param a
	 */
	public PhotoAlbum(PhotoAlbum a) {
		this.name = a.name;
		this. albumImage = a.albumImage;
		this.photoList = a.photoList;
		this.nameList = a.nameList;
        this.currentPhoto = a.currentPhoto;
	}
	
	/**
	 * @param albumTitle
	 */
	public void setName(String albumTitle) {
		name = albumTitle;
	}
	
	/**
	 * @return The name of the album
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Change the name of the album
	 * @param s
	 */
	public void changeName(String s) {
		name = s;
		
	}
	
	/**
	 * Add photo to album
	 * @param photo
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void addPhoto(Photo photo) throws ClassNotFoundException, IOException {
		UserList.getCurrentUser().getCurrentAlbum().photoList.add(photo);
		UserList.getCurrentUser().getCurrentAlbum().nameList.add(photo.getName());
		UserList.saveToDAT();
	}
	
	/**
	 * @return List of the photos in the album
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public ArrayList<Photo> getPhotoList() throws ClassNotFoundException, IOException{
		UserList.getFromDAT();;
		return photoList;
		
	}
	
	/**
	 * Sets the curent image
	 * @param photo
	 */
	public void setCurrentImage(Photo photo)
	{
		currentPhoto = photo;
	}
	
	/**
	 * @return The current image
	 */
	public Photo getCurrentImage()
	{
		return currentPhoto;
	}
	
	/**
	 * @return The album image
	 */
	public ImageView getAlbumImage() {
		return this.albumImage;
	}
	
    /**
     * Add photo to a album
     * @param photo
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public void addPhotoToAlbum(Photo photo) throws ClassNotFoundException, IOException {
        
		UserList.getCurrentUser().getDestinationAlbum().photoList.add(photo);
		UserList.getCurrentUser().getDestinationAlbum().nameList.add(photo.getName());
        UserList.saveToDAT();
    }

	

}
