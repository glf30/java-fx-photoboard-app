package photos.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.Optional;
import java.util.Set;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import photos.model.UserList;

/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 */

public class LoginController {

	
	
	@FXML private AnchorPane rootPane;
    @FXML private MenuBar menu;
    @FXML private TextField userID;
    @FXML private Button login;

    public String currentUser = "";
    public static UserList ulo = new UserList();

    /**
     * Exit the program
     * @param event
     */
    @FXML
    void doExit(ActionEvent event) {
    	Platform.exit();
    }
    
    /**
     * Logs you in
     * @param event
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws ParseException
     */
    @FXML
    void doLogin(ActionEvent event) throws IOException, ClassNotFoundException, ParseException {
    	String user = userID.getText();
    	userID.clear();
    	SwitchController sceneSwitcher = new SwitchController((Stage)((Node)event.getSource()).getScene().getWindow());
    	Set<String> userList = AdminSettingsController.getUsers();
    	if(user.equalsIgnoreCase("admin"))
    	{
    		sceneSwitcher.switchScene("/photos/view/AdminSettings.fxml");    		
    	}
    	else if(userList.contains(user))
    	{
    		UserList.getFromDAT();
    		UserList.setCurrentUser(user);
    		sceneSwitcher.switchScene("/photos/view/AlbumHome.fxml");    		
    	}
    	else
    	{
    		Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("ERROR: User not found.");
            Optional<ButtonType> alertResult = alert.showAndWait();
            if (alertResult.get() == ButtonType.OK){
                
            }
    	}
    	
    }

    /**
     * Logs you in
     * @param event
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws ParseException
     */
    @FXML
    void userIDKeyPressed(KeyEvent event) throws IOException, ClassNotFoundException, ParseException {
    	if(event.getCode().equals(KeyCode.ENTER))
    	{
    		String user = userID.getText();
        	userID.clear();
        	SwitchController sceneSwitcher = new SwitchController((Stage)((Node)event.getSource()).getScene().getWindow());
        	Set<String> userList = AdminSettingsController.getUsers();
        
        	if(user.equalsIgnoreCase("admin"))
        	{
        		sceneSwitcher.switchScene("/photos/view/AdminSettings.fxml");    		
        	}
        	else if(userList.contains(user))
        	{
        		UserList.getFromDAT();
        		UserList.setCurrentUser(user);
        		sceneSwitcher.switchScene("/photos/view/AlbumHome.fxml");    		
        	}
        	else
        	{
        		Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("ERROR: User not found.");
                Optional<ButtonType> alertResult = alert.showAndWait();
                if (alertResult.get() == ButtonType.OK){
                    
                }
        	}
    	}
    }

	/**
	 * @param primaryStage
	 */
	public void start(Stage primaryStage) {
		
	}

}
