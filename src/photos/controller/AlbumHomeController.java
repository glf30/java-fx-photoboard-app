package photos.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Optional;
import java.util.StringTokenizer;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import photos.model.Photo;
import photos.model.PhotoAlbum;
import photos.model.Tag;
import photos.model.User;
import photos.model.UserList;

/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 */

public class AlbumHomeController {
	
	
	@FXML MenuItem addAlbum;
	@FXML MenuItem renameAlbum;
	@FXML MenuItem deleteAlbum;
	@FXML MenuItem createFromSearch;
	@FXML MenuItem logout;
	@FXML MenuItem closeWindow;
    @FXML MenuBar fileMenu;
	@FXML TextField searchBar;
	@FXML Button searchButton; 
	@FXML GridPane albumGrid;
	@FXML Button testChangeScene;
	@FXML Button logoutButton;
    @FXML private TableView<Photo> resultList = new TableView<Photo>();
    @FXML private TableColumn<Photo,String> nameField = new TableColumn<Photo, String>("Name");
    @FXML private TableColumn<Photo,String> albumNameField = new TableColumn<Photo, String>("Album");
    
	Label selected = new Label();
	private  int x = 0;
	private  int y = 0;
	public User currentUser = new User("");
	public PhotoAlbum currentAlbum = new PhotoAlbum();	
    private ObservableList<Photo> resultView;
    private ObservableList<Photo> emptyView;
    boolean resultsFilled = false;
	
	/**
	 * Sets up the necessary information
	 * @param mainStage
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void start(Stage mainStage) throws IOException, ClassNotFoundException {  
		
			nameField.setCellValueFactory(new PropertyValueFactory<>("name"));
			albumNameField.setCellValueFactory(new PropertyValueFactory<>("album"));
			resultView = FXCollections.observableArrayList();
			emptyView = FXCollections.observableArrayList();
			emptyView.add(new Photo (""));
			ScrollPane scroll = new ScrollPane(albumGrid);
			scroll.setFitToWidth(true);
			scroll.setPrefSize(380, 300);
			scroll.setFitToHeight(true);
			setUser();
			loadAlbums();
		   
	}
	
	/**
	 * Sets the current User
	 */
	public void setUser() {
		
		currentUser = UserList.getCurrentUser();
	}
	
	/**
	 * Exits the program
	 * @param e
	 */
	public void doExit(ActionEvent e) {
		Platform.exit();
		
	}
	
	/**
	 * Creates an album from search results
	 * @param e
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void createAlbumFromSearch(ActionEvent e) throws ClassNotFoundException, IOException {
		
		if(resultsFilled = true) {
		
		
		TextInputDialog dialog = new TextInputDialog("");
    	dialog.setTitle("Create New Album");
    	dialog.setHeaderText(null);
    	dialog.setGraphic(null);
    	dialog.setContentText("Enter Album Name:");
    	Optional<String> result = dialog.showAndWait();
    	
    	if (result.isPresent()){
    		if(currentUser.nameList.contains(result.get())){
    			 Alert alert = new Alert(AlertType.ERROR);
    	            alert.setTitle("Error");
    	            alert.setHeaderText(null);
    	            alert.setContentText("ERROR: Duplicate Album.");

    	            Optional<ButtonType> alertResult = alert.showAndWait();
    	            if (alertResult.get() == ButtonType.OK){
    	                
    	            }
    			
    		}
    		else
    		{
    			PhotoAlbum album = new PhotoAlbum();	
    			album.setName(result.get());		
    			Label albumTitle = new Label(album.getName());
    			ImageView albumImage = new ImageView(new Image(("file:color image.jpg")));
    			albumImage.setFitHeight(70);
    			albumImage.setFitWidth(80);
    			albumTitle.setGraphic(albumImage);
    			albumTitle.setAlignment(Pos.BOTTOM_CENTER);
    			albumTitle.setContentDisplay(ContentDisplay.TOP);
    			
    			if(y >= 5 && x == 0) {
    				albumGrid.addRow(y);
    			}
    			
    			GridPane.setConstraints(albumTitle, x, y);
    			albumGrid.getChildren().add(albumTitle) ;
    			
    			
    			for(Photo p : resultView) {
    				UserList.getCurrentUser().setCurrentAlbum(album);
    				Photo photo = new Photo(p,album.getName());
    				album.addPhoto(photo);
    			}
    			
    			resultView = FXCollections.observableArrayList();
    			resultList.setItems(emptyView);
    			resultsFilled = false;
    			currentUser.addAlbum(album);	
    			albumTitle.addEventHandler(MouseEvent.MOUSE_CLICKED,  new EventHandler<MouseEvent>() {
    		           @Override
    		            public void handle(MouseEvent c) { 
    		        	  try {
    		        		  selected.setStyle("-fx-background-color:transparent;");
    		        		  albumTitle.setStyle("-fx-background-color:dodgerblue;"); 
    		        		  selected = albumTitle;
    		        		  currentAlbum = album;
    						testSceneChange(c,album);
    						
    					} catch (IOException e) {
    						e.printStackTrace();
    					} catch (ClassNotFoundException e) {
    						e.printStackTrace();
    					} catch (ParseException e) {
							e.printStackTrace();
						}
    		            		
    		            	}
    		            	
    		            	
    		           
    		        }); 
    		

    			
    			x++;
    			if(x >= 4) {
    				y++;
    				x = 0;
    			}

    			
    		}
    	  }
		}
	}
	
	/**
	 * Search button
	 * @param e
	 * @throws ParseException
	 */
	public void searchEnter(ActionEvent e) throws ParseException {
		String search = "";
		String type = "";
		String value = "";
		String before = "";
		String after = "";
	    resultView = FXCollections.observableArrayList();
		resultList.setItems(emptyView);
		resultsFilled = false;
		
		if(!(searchBar.getText().isEmpty())){
			search = searchBar.getText();
			int eCount = 0;
			for( int i=0; i<search.length(); i++ ) {
			    if( search.charAt(i) == '=' ) {
			        eCount++;
			    } 
			}
			StringTokenizer st = new StringTokenizer(search,",");
			StringTokenizer st2 = new StringTokenizer(search,",");
			String orig = search;
			search = st.nextToken();
			int tCount = 0;
			st2.nextToken();
		
			if(search.contains("=")){
				tCount = 0;
				type = search.substring(0, search.indexOf("="));
				value = search.substring(search.indexOf("=") + 1);
				for(PhotoAlbum a : currentUser.albumList) {
					for(Photo p : a.photoList) {
						//resultView = FXCollections.observableArrayList();;
						tCount = 0;
						st = new StringTokenizer(orig,",");
						search = st.nextToken();
						st2 = new StringTokenizer(orig,",");
						//st2.nextToken();
						int tagIt = 0;
						
						int psize = p.tagList.size();
						
						while(tagIt< psize) {
							
							Tag tag = p.tagList.get(tagIt);
							if(tag.getType().toLowerCase().equals(type.toLowerCase().trim()) 
									&& tag.getValue().toLowerCase().equals(value.toLowerCase().trim())) {
								tCount++; 
								
								if(eCount != 1 && tCount<eCount && p.tagList.size()>1) {  
									tCount= 0;
									while(st2.hasMoreTokens()) {
										
										search = st2.nextToken();
										type = search.substring(0, search.indexOf("="));
										value = search.substring(search.indexOf("=") + 1);
										
										for(Tag t : p.tagList) {
										if(t.getType().toLowerCase().equals(type.toLowerCase().trim()) 
												&& t.getValue().toLowerCase().equals(value.toLowerCase().trim())) {
										
											tCount++;
										
											if(eCount == tCount) {
												//System.out.println("ADDED: " + p.getName());
												resultView.add(p);
											    break;
											}
										}
										}
									}
									
								  } else {
								    if(tCount==1 && eCount == 1) {
								    	resultView.add(p);
								    	break;
								    }
								  }
							}	
							tagIt++;
						}
					}
				}
				
			}
			search = orig;
			if(search.contains("-")) {
				before = search.substring(0, search.indexOf("-"));
				after = search.substring(search.indexOf("-") + 1);
				for(PhotoAlbum a : currentUser.albumList) {
					for(Photo p : a.photoList) {
						 Date beforeDate =new SimpleDateFormat("MM/dd/yyyy").parse(before.trim());
						 Date afterDate =new SimpleDateFormat("MM/dd/yyyy").parse(after.trim());
						
						   if(!beforeDate.after(p.getCalDate()) && !afterDate.before(p.getCalDate())) {
							   resultView.add(p);
							      
							}
					}
				}
				
				
			}
			/*types of searches 
			 * DATE
			 * KEY = VALUE
			
			 */
			
			 if(resultView.size() > 0) {
			  resultList.setItems(resultView); 
			  resultsFilled = true;
	        } else {
	        	
	        	resultList.setItems(emptyView);
	        	resultsFilled = false;
	        }
			
			searchBar.clear();
		}
		
		
	}
	
	/**
	 * Search enter key
	 * @param e
	 * @throws ParseException
	 */
	public void searchEnterKey(KeyEvent e) throws ParseException {
		//String search = "";
		if(!(searchBar.getText().isEmpty()) && e.getCode() == KeyCode.ENTER){
			String search = "";
			String type = "";
			String value = "";
			String before = "";
			String after = "";
		    resultView = FXCollections.observableArrayList();
			resultList.setItems(emptyView);
			resultsFilled = false;
			
			if(!(searchBar.getText().isEmpty())){
				search = searchBar.getText();
				int eCount = 0;
				for( int i=0; i<search.length(); i++ ) {
				    if( search.charAt(i) == '=' ) {
				        eCount++;
				    } 
				}
				StringTokenizer st = new StringTokenizer(search,",");
				StringTokenizer st2 = new StringTokenizer(search,",");
				String orig = search;
				search = st.nextToken();
				int tCount = 0;
				st2.nextToken();
			
				if(search.contains("=")){
					tCount = 0;
					type = search.substring(0, search.indexOf("="));
					value = search.substring(search.indexOf("=") + 1);
					for(PhotoAlbum a : currentUser.albumList) {
						for(Photo p : a.photoList) {
							//resultView = FXCollections.observableArrayList();;
							tCount = 0;
							st = new StringTokenizer(orig,",");
							search = st.nextToken();
							st2 = new StringTokenizer(orig,",");
							//st2.nextToken();
							int tagIt = 0;
							
							int psize = p.tagList.size();
							
							while(tagIt< psize) {
								
								Tag tag = p.tagList.get(tagIt);
								if(tag.getType().toLowerCase().equals(type.toLowerCase().trim()) 
										&& tag.getValue().toLowerCase().equals(value.toLowerCase().trim())) {
									tCount++; 
									
									if(eCount != 1 && tCount<eCount && p.tagList.size()>1) {  
										tCount= 0;
										while(st2.hasMoreTokens()) {
											
											search = st2.nextToken();
											type = search.substring(0, search.indexOf("="));
											value = search.substring(search.indexOf("=") + 1);
											
											for(Tag t : p.tagList) {
											if(t.getType().toLowerCase().equals(type.toLowerCase().trim()) 
													&& t.getValue().toLowerCase().equals(value.toLowerCase().trim())) {
											
												tCount++;
											
												if(eCount == tCount) {
													//System.out.println("ADDED: " + p.getName());
													resultView.add(p);
												    break;
												}
											}
											}
										}
										
									  } else {
									    if(tCount==1 && eCount == 1) {
									    	resultView.add(p);
									    	break;
									    }
									  }
								}	
								tagIt++;
							}
						}
					}
					
				}
				search = orig;
				if(search.contains("-")) {
					before = search.substring(0, search.indexOf("-"));
					after = search.substring(search.indexOf("-") + 1);
					for(PhotoAlbum a : currentUser.albumList) {
						for(Photo p : a.photoList) {
							 Date beforeDate =new SimpleDateFormat("MM/dd/yyyy").parse(before.trim());
							 Date afterDate =new SimpleDateFormat("MM/dd/yyyy").parse(after.trim());
							
							   if(!beforeDate.after(p.getCalDate()) && !afterDate.before(p.getCalDate())) {
								   resultView.add(p);
								      
								}
						}
					}
					
					
				}
				/*types of searches 
				 * DATE
				 * KEY = VALUE
				
				 */
				
				 if(resultView.size() > 0) {
				  resultList.setItems(resultView); 
				  resultsFilled = true;
		        } else {
		        	
		        	resultList.setItems(emptyView);
		        	resultsFilled = false;
		        }
				
				searchBar.clear();
			}
			
		}
			
			
			
			
		
		
		
	}
	
	/**
	 * Logs you out the system
	 * @param e
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws ParseException
	 */
	public void logoutOfSystem(ActionEvent e) throws IOException, ClassNotFoundException, ParseException {
		//transition to logout screen
		x = 0;
		y = 0; 
		SwitchController sceneSwitcher = new SwitchController((Stage)fileMenu.getScene().getWindow());
    	sceneSwitcher.switchScene("/photos/view/Login.fxml");
	}
	
	/**
	 * Loads the albums
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void loadAlbums() throws ClassNotFoundException, IOException {
		
		ArrayList<PhotoAlbum> currentAlbumList = currentUser.albumList;
		if(currentAlbumList == null) {
			currentUser.albumList = new ArrayList<PhotoAlbum>();
			currentAlbumList = currentUser.albumList;
		}
		int albumCount = 0;
		for (Iterator<PhotoAlbum> it = currentAlbumList.iterator(); it.hasNext(); ) {
			
			 PhotoAlbum album = it.next(); 
		
			 ImageView albumImage = new ImageView(new Image(("file:color image.jpg")));
				albumImage.setFitHeight(70);
				albumImage.setFitWidth(80);		
			Label albumTitle = new Label(album.getName());
			
			
			albumTitle.setGraphic(albumImage);
			albumTitle.setAlignment(Pos.BOTTOM_CENTER);
			albumTitle.setContentDisplay(ContentDisplay.TOP);
			
			if(y >= 5 && x == 0) {
				albumGrid.addRow(y);
				
			}
			
			GridPane.setConstraints(albumTitle, x, y);
			albumGrid.getChildren().add(albumTitle) ;
			//albumTitle.setStyle("-fx-background-color:yellow;");
			albumCount++;
			if(albumCount == 1) {
				currentAlbum = album;
				selected = albumTitle;
				
				albumTitle.setStyle("-fx-background-color:dodgerblue;"); 	
			}
			//currentUser.addToAlbum(album);	
			albumTitle.addEventHandler(MouseEvent.MOUSE_CLICKED,  new EventHandler<MouseEvent>() {
		           @Override
		            public void handle(MouseEvent c) { 
		        	  try {
		        		 selected.setStyle("-fx-background-color:transparent;");
		        		  albumTitle.setStyle("-fx-background-color:dodgerblue;"); 
		        		  selected = albumTitle;
		        		  currentAlbum = album;
						testSceneChange(c,album);
						
					} catch (IOException e) {
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					} catch (ParseException e) {
						e.printStackTrace();
					}
		            		
		            	}
		            	
		            	
		           
		        }); 
		
	
			
			x++;
			if(x >= 4) {
				y++;
				x = 0;
			}
			//albumCount++;
	    }	
		
	}
	
	/**
	 * Add album button
	 * @param e
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void addAlbum(ActionEvent e) throws IOException, ClassNotFoundException  {
		TextInputDialog dialog = new TextInputDialog("");
    	dialog.setTitle("Create New Album");
    	dialog.setHeaderText(null);
    	dialog.setGraphic(null);
    	dialog.setContentText("Enter Album Name:");
    	Optional<String> result = dialog.showAndWait();
    	
    	if (result.isPresent()){
    		if(currentUser.nameList.contains(result.get())){
    			 Alert alert = new Alert(AlertType.ERROR);
    	            alert.setTitle("Error");
    	            alert.setHeaderText(null);
    	            alert.setContentText("ERROR: Duplicate Album.");

    	            Optional<ButtonType> alertResult = alert.showAndWait();
    	            if (alertResult.get() == ButtonType.OK){
    	                
    	            }
    			
    		}
    		else
    		{
    			//currentUser.albumList. .add(result.get());
    			
    			PhotoAlbum album = new PhotoAlbum();
    			
    			album.setName(result.get());		
    			Label albumTitle = new Label(album.getName());
    			
    			ImageView albumImage = new ImageView(new Image(("file:color image.jpg")));
    			albumImage.setFitHeight(70);
    			albumImage.setFitWidth(80);
    			
    			albumTitle.setGraphic(albumImage);
    			albumTitle.setAlignment(Pos.BOTTOM_CENTER);
    			albumTitle.setContentDisplay(ContentDisplay.TOP);
    			
    			if(y >= 5 && x == 0) {
    				albumGrid.addRow(y);
    			}
    			GridPane.setConstraints(albumTitle, x, y);
    			albumGrid.getChildren().add(albumTitle) ;
    			currentUser.addAlbum(album);	
    			albumTitle.addEventHandler(MouseEvent.MOUSE_CLICKED,  new EventHandler<MouseEvent>() {
    		           @Override
    		            public void handle(MouseEvent c) { 
    		        	  try {
    		        		  selected.setStyle("-fx-background-color:transparent;");
    		        		  albumTitle.setStyle("-fx-background-color:dodgerblue;"); 
    		        		  selected = albumTitle;
    		        		  currentAlbum = album;
    						testSceneChange(c,album);
    						
    					} catch (IOException e) {
    						e.printStackTrace();
    					} catch (ClassNotFoundException e) {
    						e.printStackTrace();
    					} catch (ParseException e) {
							e.printStackTrace();
						}
    		            		
    		            	}
    		            	
    		            	
    		           
    		        }); 
    		

    			
    			x++;
    			if(x >= 4) {
    				y++;
    				x = 0;
    			}

    			
    		}
    	}	  	
	}
	
	/**
	 * Renames the current album
	 * @param e
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void renameAlbum(ActionEvent e) throws ClassNotFoundException, IOException, ParseException {
		//rename album TEMPORARY
		
		TextInputDialog dialog = new TextInputDialog("");
		
    	dialog.setTitle("Add New Photo");
    	dialog.setHeaderText(null);
    	dialog.setGraphic(null);
    	dialog.setContentText("Enter New Album Name:");
    	Optional<String> result = dialog.showAndWait();
  
    	if (result.isPresent()){
    		if(currentUser.nameList.contains(result.get())){
    			 Alert alert = new Alert(AlertType.ERROR);
    	            alert.setTitle("Error");
    	            alert.setHeaderText(null);
    	            alert.setContentText("ERROR: Duplicate Album.");

    	            Optional<ButtonType> alertResult = alert.showAndWait();
    	            if (alertResult.get() == ButtonType.OK){
    	                
    	            }
    			
    		}
    		else
    		{
    			//currentUser.albumList. .add(result.get());
    			String s = currentAlbum.getName();
    		
    		
    			PhotoAlbum rename = currentAlbum;
    			rename.changeName(result.get());
    			
    			currentUser.nameList.set(currentUser.nameList.indexOf(s), result.get());
    			currentAlbum.changeName(result.get());
    			//int index = currentUser.albumList.indexOf(currentAlbum);
    			currentUser.albumList.set(currentUser.albumList.indexOf(currentAlbum), rename);
    			selected.setText(result.get());
    			
	            for(Photo p : currentAlbum.photoList) {
    				
    				p.setAlbum(result.get());
    			}
    			
    			UserList.saveToDAT();
    			x = 0;
    			y = 0;
    			SwitchController sceneSwitcher = new SwitchController((Stage)fileMenu.getScene().getWindow());
    	    	sceneSwitcher.switchScene("/photos/view/AlbumHome.fxml");
    			
    		}
    	}
		
		
	
	}

	/**
	 * Deletes the current album
	 * @param e
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void deleteAlbum(ActionEvent e) throws ClassNotFoundException, IOException, ParseException {
		currentUser.nameList.remove(currentAlbum.getName());
		currentUser.albumList.remove(currentAlbum);
		
		UserList.saveToDAT();
		x = 0;
		y = 0;
		SwitchController sceneSwitcher = new SwitchController((Stage)fileMenu.getScene().getWindow());
    	sceneSwitcher.switchScene("/photos/view/AlbumHome.fxml");
		
	}
	
	/**
	 * Changes the scene
	 * @param e
	 * @param album
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws ParseException
	 */
	public void testSceneChange(MouseEvent e, PhotoAlbum album) throws IOException, ClassNotFoundException, ParseException {
		
		SwitchController sceneSwitcher = new SwitchController((Stage)((Node)e.getSource()).getScene().getWindow());
		
		if(e.getClickCount() == 2) {
			x = 0;
			y = 0;
			currentUser.setCurrentAlbum(album);
			sceneSwitcher.switchScene("/photos/view/AlbumPhotos.fxml");
		}
	
	}
	
}