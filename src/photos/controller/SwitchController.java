package photos.controller;


import java.io.IOException;
import java.text.ParseException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 */

public class SwitchController {
	
	public Stage stage;
	
	
	/**
	 * Sets the primary stage as the SwitchController's stage
	 * @param primaryStage
	 * @throws IOException
	 */
	public SwitchController(Stage primaryStage) throws IOException
	{
		this.stage = primaryStage;
	}

	/**
	 * Sets the primary stage. Only used at the beginning
	 * @throws IOException
	 */
	public void setPrimaryStage() throws IOException
	{		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/photos/view/login.fxml"));
		Parent root = (Parent)loader.load();
	    LoginController loginController = loader.getController();
		loginController.start(this.stage);
		Scene scene = new Scene(root);
		this.stage.setScene(scene);
		this.stage.setTitle("Photo Albums Login");  
		this.stage.setResizable(false);
		this.stage.show();
	}
	
	/**
	 * Changes the scene to the FXML file given
	 * @param fxmlFile
	 * @throws ClassNotFoundException
	 * @throws ParseException
	 */
	public void switchScene(String fxmlFile) throws ClassNotFoundException, ParseException
	{
        
	    FXMLLoader loader = new FXMLLoader();
	    loader.setLocation(getClass().getResource(fxmlFile));
	    Parent root;
	    try 
	    {
	        root = (Parent)loader.load();
	        if(fxmlFile.equals("/photos/view/Login.fxml"))
	        {
	            LoginController controller = (LoginController)loader.getController();
	            this.stage.setTitle("Photo Albums Login"); 
	            controller.start(this.stage);
	        }
	        else if(fxmlFile.equals("/photos/view/AdminSettings.fxml"))
	        {
	            AdminSettingsController controller = (AdminSettingsController)loader.getController();
	            this.stage.setTitle("Admin Settings"); 
	            controller.start(this.stage);    
	        }
	        else if(fxmlFile.equals("/photos/view/AlbumHome.fxml"))
	        {
	        	AlbumHomeController controller = (AlbumHomeController)loader.getController();
	        	this.stage.setTitle("Photo Albums Home"); 
	        	controller.start(this.stage);
	        }
	        else if(fxmlFile.equals("/photos/view/AlbumPhotos.fxml"))
	        {
	        	AlbumPhotosController controller = (AlbumPhotosController)loader.getController();
	        	this.stage.setTitle("Photo Albums Photos"); 
	        	controller.start(this.stage);
	        }
	        else if(fxmlFile.equals("/photos/view/PictureView.fxml"))
	        {
	        	PictureViewController controller = (PictureViewController)loader.getController();
	        	this.stage.setTitle("Gallery"); 
	        	controller.start(this.stage);
	        }
	        this.stage.setScene(new Scene(root));
	        this.stage.show();
	    } 
	    catch (IOException e)
	    {
	        e.printStackTrace();
	    }
	}
}
