package photos.controller;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import photos.model.Photo;
import photos.model.UserList;

/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 */

public class ConfirmPhotoController {

    @FXML private AnchorPane addPhotoScreen;
    @FXML private Button browseButton;
    @FXML private Button confirmButton;
    @FXML private Button cancelButton;
    @FXML private TextField nameField;
    @FXML private TextField captionField;
    @FXML private Label browsePath;
      
    Calendar cal = Calendar.getInstance();  
    AlbumPhotosController a = new AlbumPhotosController();
    
    /**
     * Browses using the system explorer
     * @param e
     */
    public void browse(ActionEvent e) {
    	Stage stage = new Stage();
		
		FileChooser fileChooser = new FileChooser();
		
		File file = fileChooser.showOpenDialog(stage);
		
		
		String path = file.getAbsolutePath();
        
	
    	if (file != null) {
          
        	
    		
    		
        	 a.currentPhoto = new Photo(path);
        	browsePath.setText(path);
    	}
    	
    }
    
    /**
     * Confirms and creates the photo
     * @param e
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public void confirmOption(ActionEvent e) throws ClassNotFoundException, IOException{
    	
        String result = nameField.getText();
    	
       
    		if(UserList.getCurrentUser().getCurrentAlbum().nameList.contains(result) && !(nameField.getText().isEmpty())){
    			
    			 Alert alert = new Alert(AlertType.ERROR);
    	            alert.setTitle("Error");
    	            alert.setHeaderText(null);
    	            alert.setContentText("ERROR: Duplicate Name");

    	            Optional<ButtonType> alertResult = alert.showAndWait();
    	            if (alertResult.get() == ButtonType.OK){
    	                
    	            }
    	        	
    		} else {
    	
    	
    	if(nameField.getText().isEmpty() || (a.currentPhoto.getPath().isEmpty())) {
    		
    		
    		 Alert alert = new Alert(AlertType.ERROR);
	            alert.setTitle("Error");
	            alert.setHeaderText(null);
	            alert.setContentText("ERROR: Missing 1 or More recquired elements");

	            Optional<ButtonType> alertResult = alert.showAndWait();
	            if (alertResult.get() == ButtonType.OK){
	                
	            }
    		
    		
    		
    		
    	} else {	
    	a.currentPhoto.setName(nameField.getText());
    	UserList.getCurrentUser().getCurrentAlbum().getCurrentImage().setName(nameField.getText());
    	
    	
    	
    	
    	
    	a.currentPhoto.setCaption(captionField.getText());
    	
    	File file = new File(a.currentPhoto.getPath());
    	Image image = new Image(file.toURI().toString());
    	//Photo photo = new Photo(path);
    	
        ImageView newPhoto = new ImageView(image);
        
        
        Label photoTitle = new Label(a.currentPhoto.getName());
		
        
        
        
		photoTitle.setGraphic(newPhoto);
		photoTitle.setAlignment(Pos.BOTTOM_CENTER);
		photoTitle.setContentDisplay(ContentDisplay.TOP);
		
		newPhoto.setFitHeight(70);
		newPhoto.setFitWidth(80);
        GridPane.setConstraints(photoTitle, a.x, a.y);

		 
		  int year = cal.get(Calendar.YEAR);
	      int month = cal.get(Calendar.MONTH) + 1;      
	      int day = cal.get(Calendar.DAY_OF_MONTH);
	      cal.set(Calendar.MILLISECOND,0);
	      Date thisdate = cal.getTime();
		   String dateString = Integer.toString(month) +"/" + Integer.toString(day) + "/" + Integer.toString(year);
		   
		  int hour = cal.get(Calendar.HOUR);
		  int minute = cal.get(Calendar.MINUTE);
		  if(hour == 0) {
			  hour = 12;
		  }
		  
		  String timeString = Integer.toString(hour) + ":" + Integer.toString(minute);
		  
		  if(minute<10) {
			  timeString = Integer.toString(hour) + ":0" + Integer.toString(minute);
		  }
		  
		  a.currentPhoto.setTime(timeString); 
		   a.currentPhoto.setCalDate(thisdate);
		   a.currentPhoto.setDate(dateString);
		   a.currentPhoto.setAlbum(UserList.getCurrentUser().getCurrentAlbum().getName());
	
		
	
		   
		   UserList.getCurrentUser().getCurrentAlbum().addPhoto(a.currentPhoto);
		
		
		
		
		
		photoTitle.addEventHandler(MouseEvent.MOUSE_CLICKED,  new EventHandler<MouseEvent>() {
	           @Override
	            public void handle(MouseEvent c) { 
	        	  a.currentPath = a.currentPhoto.getPath();
	            	}
	            	
	            	
	           
	        }); 
	
    	}
    	
		Stage stage = (Stage) confirmButton.getScene().getWindow();
        stage.close();
    	
    		}
    }
    
    /**
     * Exit the window
     * @param e
     */
    public void doExit(ActionEvent e) {
    	Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
	}

  
  

}
