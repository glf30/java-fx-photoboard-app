package photos.controller;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Optional;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import photos.model.Photo;
import photos.model.UserList;

/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 */

public class PictureViewController {

    @FXML private MenuBar menu;
    @FXML private MenuItem albumsHome;
    @FXML private ImageView mainImage = new ImageView();
    @FXML private Label nameField;
    @FXML private Label descriptionField;
    @FXML private Label photoDate;
    @FXML private Button copyButton;
    @FXML private Button moveButton;
    @FXML private Button prev;
    @FXML private Button homeButton;
    @FXML private Button next;
    
    private String currentPath = "";

    /**
     * Exit the program
     * @param event
     */
    @FXML
    void doExit(ActionEvent event) {
    	Platform.exit();
    }
   
    /**
     * Moves the photo to another album
     * @param event
     * @throws ClassNotFoundException
     * @throws IOException
     * @throws ParseException
     */
    @FXML
    void moveToAlbum(ActionEvent event) throws ClassNotFoundException, IOException, ParseException {
   	 ArrayList<Photo> photoList = UserList.getCurrentUser().getCurrentAlbum().photoList;
	      TextInputDialog dialog = new TextInputDialog("");
	    		
	        	dialog.setTitle("Move To Album");
	        	dialog.setHeaderText(null);
	        	dialog.setGraphic(null);
	        	dialog.setContentText("Please enter destination album:");
	        	Optional<String> result = dialog.showAndWait();
	            
	        	if (result.isPresent()){
	        		if(!UserList.getCurrentUser().nameList.contains(result.get())){
	        			 Alert alert = new Alert(AlertType.ERROR);
	        	            alert.setTitle("Error");
	        	            alert.setHeaderText(null);
	        	            alert.setContentText("ERROR: Album Doesn't Exist");

	        	            Optional<ButtonType> alertResult = alert.showAndWait();
	        	            if (alertResult.get() == ButtonType.OK){
	        	                
	        	            }
	        		} else {    
	                int currentIndex = photoList.indexOf(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage());
	                int indexOfAlbumToMoveTo = UserList.getCurrentUser().nameList.indexOf(result.get());  
	                Photo photoToMove = new Photo(photoList.get(currentIndex), UserList.getCurrentUser().albumList.get(indexOfAlbumToMoveTo).getName() );
	                UserList.getCurrentUser().setDestinationAlbum(UserList.getCurrentUser().albumList.get(indexOfAlbumToMoveTo));
	                UserList.getCurrentUser().getDestinationAlbum().addPhotoToAlbum(photoToMove);
	                UserList.getCurrentUser().getCurrentAlbum().nameList.remove(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage().getName());
	                UserList.getCurrentUser().getCurrentAlbum().photoList.remove(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage());
	                SwitchController sceneSwitcher = new SwitchController((Stage)menu.getScene().getWindow());
	                sceneSwitcher.switchScene("/photos/view/AlbumPhotos.fxml");
           }
       }
   }
   
    /**
     * Copies the photo to another album
     * @param event
     * @throws ClassNotFoundException
     * @throws IOException
     */
    @FXML 
	void copyPhoto(ActionEvent event) throws ClassNotFoundException, IOException {
     ArrayList<Photo> photoList = UserList.getCurrentUser().getCurrentAlbum().photoList;
     TextInputDialog dialog = new TextInputDialog("");
   		
       	dialog.setTitle("Copy To Album");
       	dialog.setHeaderText(null);
       	dialog.setGraphic(null);
       	dialog.setContentText("Please enter destination album:");
       	Optional<String> result = dialog.showAndWait();
           
       	if (result.isPresent()){
       		if(!UserList.getCurrentUser().nameList.contains(result.get())){
       			 Alert alert = new Alert(AlertType.ERROR);
       	            alert.setTitle("Error");
       	            alert.setHeaderText(null);
       	            alert.setContentText("ERROR: Album Doesn't Exist");

       	            Optional<ButtonType> alertResult = alert.showAndWait();
       	            if (alertResult.get() == ButtonType.OK){
       	                
       	            }
       		} else {    
              int currentIndex = photoList.indexOf(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage());
              int indexOfAlbumToMoveTo = UserList.getCurrentUser().nameList.indexOf(result.get());
              Photo photoToMove = new Photo(photoList.get(currentIndex), UserList.getCurrentUser().albumList.get(indexOfAlbumToMoveTo).getName() );
              UserList.getCurrentUser().setDestinationAlbum(UserList.getCurrentUser().albumList.get(indexOfAlbumToMoveTo));
              UserList.getCurrentUser().getDestinationAlbum().addPhotoToAlbum(photoToMove);
       
    
           }
       }
		
	
   }
   
    /**
     * Go to albums button
     * @param event
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws ParseException
     */
    @FXML
    void goToAlbums(ActionEvent event) throws IOException, ClassNotFoundException, ParseException {
    	SwitchController sceneSwitcher = new SwitchController((Stage)menu.getScene().getWindow());
    	sceneSwitcher.switchScene("/photos/view/AlbumPhotos.fxml");
    }

    /**
     * Next photo in the album
     * @param event
     */
    @FXML
    void nextPhoto(ActionEvent event) {
    	ArrayList<Photo> photoList = UserList.getCurrentUser().getCurrentAlbum().photoList;
    	int currentIndex = photoList.indexOf(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage());
    	if(currentIndex < photoList.size()-1)
    	{
    		currentPath = photoList.get(currentIndex+1).getPath();
    		File file = new File(currentPath);
    		if(file!= null) {
    			Image image = new Image(file.toURI().toString());
    			mainImage.setImage(image);
    			UserList.getCurrentUser().getCurrentAlbum().setCurrentImage(photoList.get(currentIndex+1));
    			nameField.setText(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage().getName());
    			descriptionField.setText(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage().getCaption());
    			photoDate.setText(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage().getDate());
    		}
    	}
    	
    }

    /**
     * Prev photo in the album
     * @param event
     */
    @FXML
    void prevPhoto(ActionEvent event) {
    	ArrayList<Photo> photoList = UserList.getCurrentUser().getCurrentAlbum().photoList;
    	int currentIndex = photoList.indexOf(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage());
    	if(currentIndex > 0)
    	{
    		currentPath = photoList.get(currentIndex-1).getPath();
    		File file = new File(currentPath);
    		if(file!= null) {
    			Image image = new Image(file.toURI().toString());
    			mainImage.setImage(image);
    			UserList.getCurrentUser().getCurrentAlbum().setCurrentImage(photoList.get(currentIndex-1));
    			nameField.setText(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage().getName());
    			descriptionField.setText(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage().getCaption());
    			photoDate.setText(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage().getDate());
    		}
    	}
    	
    }

	/**
	 * Initialize the photo view with the selected photo from AlbumPhotosController
	 * @param stage
	 */
	public void start(Stage stage) {
		currentPath = UserList.getCurrentUser().getCurrentAlbum().getCurrentImage().getPath();
		File file = new File(currentPath);
		if(file!= null) {
			Image image = new Image(file.toURI().toString());
			mainImage.setImage(image);
			nameField.setText(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage().getName());
			descriptionField.setText(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage().getCaption());
			photoDate.setText(UserList.getCurrentUser().getCurrentAlbum().getCurrentImage().getDate());
		}
	}
}
