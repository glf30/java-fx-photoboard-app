package photos.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import photos.model.Photo;
import photos.model.PhotoAlbum;
import photos.model.Tag;
import photos.model.User;
import photos.model.UserList;

/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 */

public class AlbumPhotosController implements Initializable {
	
	@FXML private MenuBar fileMenu;
	@FXML private MenuItem addPhoto;
    @FXML private MenuItem editCaption;
    @FXML private MenuItem slideshowMode;
    @FXML private MenuItem deletePhoto;
    @FXML private MenuItem copyPhoto;
    @FXML private MenuItem movePhoto;
    @FXML private MenuItem logout;
    @FXML private MenuItem closeWindow;
    @FXML private TextField searchBar;
    @FXML private Button searchButton;
    @FXML private Button addTag;
    @FXML private Button removeTag;
    @FXML private TextField tagType;
    @FXML private TextField tagValue;
    @FXML private Button backToAlbums;
    @FXML private Button createFromSearch;
    @FXML private Label photoNumber;
    @FXML private Label photoName = new Label();
    @FXML private Label photoDate;
    @FXML private Label currentCaption  = new Label();
    @FXML private GridPane photoGrid;
    @FXML private TableView<Tag> tagList = new TableView<Tag>();
    @FXML private TableColumn<Tag,String> typeField = new TableColumn<Tag, String>("Type");
    @FXML private TableColumn<Tag,String> valueField = new TableColumn<Tag, String>("Value");
    @FXML private ImageView displayPhoto = new ImageView();
    @FXML private Label dateRange;
    @FXML private Label photoTime;
    
    String currentPath = "";
    Label selected = new Label();
    private ObservableList<Tag> tagView;
    private ObservableList<Tag> emptyView;
    Calendar cal =Calendar.getInstance(); 
    Date earlyDate = cal.getTime();
    Date lateDate = cal.getTime();
    int photoCount = 0;
    int x = 0;
	int y = 0;
	public User currentUser = new User("");
	public PhotoAlbum currentAlbum = new PhotoAlbum();
	public Photo currentPhoto = new Photo("");
	
	/**
	 * Sets up the necessary information
	 * @param stage
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void start(Stage stage) throws ClassNotFoundException, IOException, ParseException { 
		
		int year = cal.get(Calendar.YEAR);
	      int month = cal.get(Calendar.MONTH) + 1;      
	      int day = cal.get(Calendar.DAY_OF_MONTH);
	      String dateString = Integer.toString(month) +"/" + Integer.toString(day) + "/" + Integer.toString(year);
	     earlyDate =new SimpleDateFormat("MM/dd/yyyy").parse(dateString);
	     lateDate =new SimpleDateFormat("MM/dd/yyyy").parse(dateString);
		
		
		typeField.setCellValueFactory(new PropertyValueFactory<>("type"));
		valueField.setCellValueFactory(new PropertyValueFactory<>("value"));
		tagView = FXCollections.observableArrayList();
		emptyView = FXCollections.observableArrayList();
		emptyView.add(new Tag ("",""));
		setAlbum();
		loadPhotos();
		
		 
		
	    ScrollPane scroll = new ScrollPane(photoGrid);
	    scroll.setFitToWidth(true);
	    scroll.setPrefSize(380, 300);
	}
	
	/**
	 * Sets the current album
	 */
	public void setAlbum() {
		
		currentAlbum = UserList.getCurrentUser().getCurrentAlbum();
	}
	
	/**
	 * Loads the photos
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void loadPhotos() throws ClassNotFoundException, IOException, ParseException {
		
		ArrayList<Photo> currentPhotoList = currentAlbum.photoList;
		if(currentPhotoList == null) {
			currentAlbum.photoList = new ArrayList<Photo>();
			currentPhotoList = currentAlbum.photoList;
		}
		photoCount = 0;
		for (Iterator<Photo> it = currentPhotoList.iterator(); it.hasNext(); ) {
			
			Photo photo = it.next();
			File file = new File(photo.getPath());
			 
			if(file!= null) {
				Image image = new Image(file.toURI().toString());
			 
			ImageView photoImage = new ImageView(image);
			photoImage.setFitHeight(70);
			photoImage.setFitWidth(80);		
			Label photoTitle = new Label(photo.getName());
			photoTitle.setGraphic(photoImage);
			photoTitle.setAlignment(Pos.BOTTOM_CENTER);
			photoTitle.setContentDisplay(ContentDisplay.TOP);
			
			
            GridPane.setConstraints(photoTitle, x, y);

			photoGrid.getChildren().add(photoTitle) ;
			Date beforeDate =new SimpleDateFormat("MM/dd/yyyy").parse(photo.getDate());
			
			Date afterDate =new SimpleDateFormat("MM/dd/yyyy").parse(photo.getDate());
			photoCount++;
			if(photoCount == 1){
				earlyDate = beforeDate;
				lateDate = afterDate;
			}
			
			
			 
			 if(beforeDate.before(earlyDate) || beforeDate.equals(earlyDate)) {
				
				 earlyDate = beforeDate;
			 }
			 if(afterDate.after(lateDate) || afterDate.equals(lateDate)) {
				 lateDate = afterDate;
			 }
			 
			setDateRange();
			if(photoCount == 1) {
				currentPhoto = photo;
				selected = photoTitle;
				currentPath = photo.getPath();
				photoName.setText(currentPhoto.getName());
				 initialize(null,null);
				 
				photoTitle.setStyle("-fx-background-color:dodgerblue;"); 	
			}
			
			photoTitle.addEventHandler(MouseEvent.MOUSE_CLICKED,  new EventHandler<MouseEvent>() {
		           @Override
		            public void handle(MouseEvent c) { 
		        	   
		        	   
		        	   selected.setStyle("-fx-background-color:transparent;");
		        	   photoTitle.setStyle("-fx-background-color:dodgerblue;"); 
		        	   selected = photoTitle;
		        	   currentPhoto = photo;
		        	   currentPath = photo.getPath();
		        	   initialize(null, null);
		        	   try {
						testSceneChange(c, currentPhoto);
					} catch (ClassNotFoundException | IOException e) {
						e.printStackTrace();
					} catch (ParseException e) {
						e.printStackTrace();
					}
		        	   
		        	   
		            		
		            	}
		            	
		            	
		           
		        }); 
		
	
			 
			x++;
			if(x >= 5) {
				y++;
				x = 0;
			   }
			 }
	    }
		
		photoNumber.setText(Integer.toString(photoCount));
		
	}
	
	/**
	 * Adds a photo to the album
	 * @param e
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void addPhoto(ActionEvent e) throws ClassNotFoundException, IOException, ParseException {
		x = 0;
		y = 0;
		FXMLLoader loader =new FXMLLoader();
		loader.setLocation(getClass().getResource("/photos/view/ConfirmPhoto.fxml"));
		Parent root = (Parent)loader.load();
		Stage stage = new Stage();
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("Add New Photo");
		stage.showAndWait(); 
		
		
	
    	
    		SwitchController sceneSwitcher = new SwitchController((Stage)fileMenu.getScene().getWindow());
        	sceneSwitcher.switchScene("/photos/view/AlbumPhotos.fxml");
    	}
	
	/**
	 * Exits the program
	 * @param e
	 */
	public void doExit(ActionEvent e) {
		Platform.exit();
		
	}
	
	/**
	 * Logs you out the system
	 * @param e
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws ParseException
	 */
	public void logoutOfSystem(ActionEvent e) throws IOException, ClassNotFoundException, ParseException {
		//transition to logout screen
		x = 0;
		y = 0;
		//UserList.saveToDAT();
		SwitchController sceneSwitcher = new SwitchController((Stage)fileMenu.getScene().getWindow());
    	sceneSwitcher.switchScene("/photos/view/Login.fxml");
	}
	
	/**
	 * Goes back to the album page
	 * @param e
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void backToAlbum(ActionEvent e) throws ClassNotFoundException, IOException, ParseException {
	
			//transition to logout screen
			x = 0;
			y = 0;
			SwitchController sceneSwitcher = new SwitchController((Stage)backToAlbums.getScene().getWindow());
	    	sceneSwitcher.switchScene("/photos/view/AlbumHome.fxml");
		
	}
	
	/**
	 * Edits the caption of the current photo
	 * @param e
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void editCaption(ActionEvent e) throws IOException, ClassNotFoundException {
		
		
	    TextInputDialog dialog = new TextInputDialog("");
		
    	dialog.setTitle("Edit Caption");
    	dialog.setHeaderText(null);
    	dialog.setGraphic(null);
    	dialog.setContentText("New Caption: ");
    	Optional<String> result = dialog.showAndWait();
        if(result.isPresent())
    	{
    		currentPhoto.setCaption(result.get());
    		currentCaption.setText(result.get());
    	}
		//SwitchController sceneSwitcher = new SwitchController((Stage)fileMenu.getScene().getWindow());
    	//sceneSwitcher.switchScene("AlbumPhotos.fxml");
	}
	
	/**
	 * Deletes the current photo
	 * @param e
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws ParseException
	 */
	public void deletePhoto(ActionEvent e) throws IOException, ClassNotFoundException, ParseException {
		
		UserList.getCurrentUser().getCurrentAlbum().nameList.remove(currentPhoto.getName());
		UserList.getCurrentUser().getCurrentAlbum().photoList.remove(currentPhoto);
		UserList.saveToDAT();
		x = 0;
		y = 0;
		SwitchController sceneSwitcher = new SwitchController((Stage)fileMenu.getScene().getWindow());
    	sceneSwitcher.switchScene("/photos/view/AlbumPhotos.fxml");
	}

	/**
	 * Adds a tag to the current photo
	 * @param e
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void addTag(ActionEvent e) throws IOException, ClassNotFoundException {
		boolean duplicates = false;
		if(!tagType.getText().isEmpty() && !tagValue.getText().isEmpty()) {
			for(Tag tag : currentPhoto.tagList) {
				if(tag.getType().equals(tagType.getText()) && tag.getValue().equals(tagValue.getText())) {
					Alert alert = new Alert(AlertType.ERROR);
	 	            alert.setTitle("Error");
	 	            alert.setHeaderText(null);
	 	            alert.setContentText("ERROR: Duplicate Tag.");

	 	            Optional<ButtonType> alertResult = alert.showAndWait();
	 	            if (alertResult.get() == ButtonType.OK){
	 	            }
	 	            duplicates = true;
	 	            break;
				}
			}
			
			//if(currentPhoto.tagList.contains(new Tag(tagType.getText(),tagValue.getText()))) {
				//currentPhoto.tagList.con 
			
 	                
 	            	
			 if(duplicates == false) {
				currentPhoto.addTag(tagType.getText(), tagValue.getText());
				
				tagType.clear();
				tagValue.clear();
				initialize(null,null);
			 }
			}

		
	}
	
	/**
	 * Removes the tag of a photo
	 * @param e
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void removeTag(ActionEvent e) throws IOException, ClassNotFoundException{
		Tag t = tagList.getSelectionModel().getSelectedItem();
		currentPhoto.tagList.remove(t);
		initialize(null,null);
		
	}
	
	/* (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	        File file = new File(currentPath);
	        Image image = new Image(file.toURI().toString());
	        displayPhoto.setImage(image);
	        currentCaption.setText(currentPhoto.getCaption());
	        photoName.setText(currentPhoto.getName());
	        photoDate.setText(currentPhoto.getDate());
	        photoTime.setText(currentPhoto.getTime());
	        setDateRange();
	        
	        if(currentPhoto.tagList.size() > 0) {
	        		tagView.setAll(currentPhoto.tagList);
	      
			   
			  tagList.setItems(tagView); 
	        } else {
	        	
	        	tagList.setItems(emptyView);
	        }
	    }

	/**
	 * Changes the scene
	 * @param e
	 * @param photo
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws ParseException
	 */
	public void testSceneChange(MouseEvent e, Photo photo) throws IOException, ClassNotFoundException, ParseException {
			
			SwitchController sceneSwitcher = new SwitchController((Stage)((Node)e.getSource()).getScene().getWindow());
			
			if(e.getClickCount() == 2) {
				x = 0;
				y = 0;
				UserList.getCurrentUser().getCurrentAlbum().setCurrentImage(photo);
				sceneSwitcher.switchScene("/photos/view/PictureView.fxml");
			}

		}
	    
	/**
	 * Moves the current photo
	 * @param event
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void moveToAlbum(ActionEvent event) throws ClassNotFoundException, IOException, ParseException {
	    	 ArrayList<Photo> photoList = UserList.getCurrentUser().getCurrentAlbum().photoList;
		      TextInputDialog dialog = new TextInputDialog("");
		    		
		        	dialog.setTitle("Move To Album");
		        	dialog.setHeaderText(null);
		        	dialog.setGraphic(null);
		        	dialog.setContentText("Please enter destination album:");
		        	Optional<String> result = dialog.showAndWait();
		            
		        	if (result.isPresent()){
		        		if(!UserList.getCurrentUser().nameList.contains(result.get())){
		        			 Alert alert = new Alert(AlertType.ERROR);
		        	            alert.setTitle("Error");
		        	            alert.setHeaderText(null);
		        	            alert.setContentText("ERROR: Album Doesn't Exist");

		        	            Optional<ButtonType> alertResult = alert.showAndWait();
		        	            if (alertResult.get() == ButtonType.OK){
		        	                
		        	            }
		        		} else {    
		               int currentIndex = photoList.indexOf(currentPhoto);
		           int indexOfAlbumToMoveTo = UserList.getCurrentUser().nameList.indexOf(result.get());
		              
		                Photo photoToMove = new Photo(photoList.get(currentIndex), UserList.getCurrentUser().albumList.get(indexOfAlbumToMoveTo).getName() );
		                UserList.getCurrentUser().setDestinationAlbum(UserList.getCurrentUser().albumList.get(indexOfAlbumToMoveTo));
		                UserList.getCurrentUser().getDestinationAlbum().addPhotoToAlbum(photoToMove);
		        
		     
		            
		        
	        		UserList.getCurrentUser().getCurrentAlbum().nameList.remove(currentPhoto.getName());
	        		UserList.getCurrentUser().getCurrentAlbum().photoList.remove(currentPhoto);
	                SwitchController sceneSwitcher = new SwitchController((Stage)fileMenu.getScene().getWindow());
	                sceneSwitcher.switchScene("/photos/view/AlbumPhotos.fxml");
	            }
	        }
	    }
	        
	/**
	 * Copies the current photo
	 * @param event
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void copyPhoto(ActionEvent event) throws ClassNotFoundException, IOException {
	      ArrayList<Photo> photoList = UserList.getCurrentUser().getCurrentAlbum().photoList;
	      TextInputDialog dialog = new TextInputDialog("");
	    		
	        	dialog.setTitle("Copy To Album");
	        	dialog.setHeaderText(null);
	        	dialog.setGraphic(null);
	        	dialog.setContentText("Please enter destination album:");
	        	Optional<String> result = dialog.showAndWait();
	            
	        	if (result.isPresent()){
	        		if(!UserList.getCurrentUser().nameList.contains(result.get())){
	        			 Alert alert = new Alert(AlertType.ERROR);
	        	            alert.setTitle("Error");
	        	            alert.setHeaderText(null);
	        	            alert.setContentText("ERROR: Album Doesn't Exist");

	        	            Optional<ButtonType> alertResult = alert.showAndWait();
	        	            if (alertResult.get() == ButtonType.OK){
	        	                
	        	            }
	        		} else {    
	               int currentIndex = photoList.indexOf(currentPhoto);
	               int indexOfAlbumToMoveTo = UserList.getCurrentUser().nameList.indexOf(result.get());
	              
	                Photo photoToMove = new Photo(photoList.get(currentIndex), UserList.getCurrentUser().albumList.get(indexOfAlbumToMoveTo).getName() );
	                UserList.getCurrentUser().setDestinationAlbum(UserList.getCurrentUser().albumList.get(indexOfAlbumToMoveTo));
	                UserList.getCurrentUser().getDestinationAlbum().addPhotoToAlbum(photoToMove);
	        
	     
	            }
	        }
			
		
	    }
	    
	/**
	 * Sets the date range
	 */
	public void setDateRange() {
		   DateFormat form = new SimpleDateFormat("MM/dd/YYYY");
	        String dateStringE = form.format(earlyDate);
	        String dateStringL = form.format(lateDate);
	        String realRange = dateStringE + " - " + dateStringL;
	        
	        if(currentAlbum.photoList.size() > 0) {
	        	dateRange.setText(realRange);
	        } else {
	        	dateRange.setText("MM/dd/YYYY - MM/dd/YYYY");
	        }
	   }
}

