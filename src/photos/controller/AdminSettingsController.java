package photos.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import photos.model.UserList;

/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 */

public class AdminSettingsController {
	
	
    @FXML private MenuBar menu;
    @FXML private ListView<String> userList;
    @FXML private Button addButton;
    @FXML private Button deleteButton;
 
	protected static Set<String> listOfUsers;
	protected ListProperty<String> listProperty;
    private void initializeList() throws ClassNotFoundException, IOException {
    
		userList.itemsProperty().bind(listProperty);
		listProperty.set(FXCollections.observableArrayList(listOfUsers));
		
	}
    
    
    /**
     * Adds a user
     * @param event
     * @throws ClassNotFoundException
     * @throws IOException
     */
    @FXML
    void addUser(ActionEvent event) throws ClassNotFoundException, IOException {
    	
    	TextInputDialog dialog = new TextInputDialog("");
    	dialog.setTitle("Add User");
    	dialog.setHeaderText(null);
    	dialog.setGraphic(null);
    	dialog.setContentText("Please enter user's name:");
    	Optional<String> result = dialog.showAndWait();
    	if (result.isPresent()){
    		if(listOfUsers.contains(result.get())){
    			 Alert alert = new Alert(AlertType.ERROR);
    	            alert.setTitle("Error");
    	            alert.setHeaderText(null);
    	            alert.setContentText("ERROR: Duplicate User.");

    	            Optional<ButtonType> alertResult = alert.showAndWait();
    	            if (alertResult.get() == ButtonType.OK){
    	                
    	            }
    			
    		}
    		else
    		{
    			listOfUsers.add(result.get());
    		}
    	}
    	//saveToDAT();
	    UserList.add(result.get());
    	listProperty.set(FXCollections.observableArrayList(listOfUsers));
    }

    /**
     * Deletes user
     * @param event
     * @throws ClassNotFoundException
     * @throws IOException
     */
    @FXML
    void deleteUser(ActionEvent event) throws ClassNotFoundException, IOException {
    	final int selectedIdx = userList.getSelectionModel().getSelectedIndex();
        if (selectedIdx != -1) {
          String itemToRemove = userList.getSelectionModel().getSelectedItem();
          if(itemToRemove.equalsIgnoreCase("admin"))
          {
        	  Alert alert = new Alert(AlertType.ERROR);
	            alert.setTitle("Error");
	            alert.setHeaderText(null);
	            alert.setContentText("ERROR: Can't delete admin.");

	            Optional<ButtonType> alertResult = alert.showAndWait();
	            if (alertResult.get() == ButtonType.OK){
	                
	            }
          }
          else
          {
        	  final int newSelectedIdx =
        	            (selectedIdx == userList.getItems().size() - 1)
        	               ? selectedIdx - 1
        	               : selectedIdx;
        	 
        	          userList.getItems().remove(selectedIdx);
        	          userList.getSelectionModel().select(newSelectedIdx);
        	          listOfUsers.remove(itemToRemove);
        	          UserList.remove(itemToRemove);
        	          //saveToDAT(); 
          }
          
        }
        
      }

    /**
     * Exits the program
     * @param event
     */
    @FXML
    void doExit(ActionEvent event) {
    	Platform.exit();
    }

    /**
     * Logs you out the system
     * @param event
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws ParseException
     */
    @FXML
    void doLogOff(ActionEvent event) throws IOException, ClassNotFoundException, ParseException {
    	SwitchController sceneSwitcher = new SwitchController((Stage)menu.getScene().getWindow());
    	sceneSwitcher.switchScene("/photos/view/Login.fxml");
    	
    }
    
    /**
     * Gets the list of users
     * @return
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static Set<String> getUsers() throws ClassNotFoundException, IOException
    {
    	
    	listOfUsers = UserList.getList();
		return listOfUsers;
    	
    }
    
    /**
     * Initializes the list with the list of users from the DAT file
     * @param primaryStage
     * @throws ClassNotFoundException
     * @throws IOException
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void start(Stage primaryStage) throws ClassNotFoundException, IOException {
		listOfUsers = new TreeSet<String>();
		listProperty = new SimpleListProperty();
    	getUsers();
    	initializeList();
		
	}
}
