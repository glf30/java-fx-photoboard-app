package photos.app;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import javafx.scene.image.Image;

public class Photo implements Serializable{
	
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7761755855823637394L;
	private String name;
	private String date;
	private String caption;
	private String photoPath;
	
	private Image photoImage;
	
	
	public ArrayList<Tag> tagList;
	
	public Photo(/*Image image,*/ String path){
		//photoImage = image;
		photoPath = path;
		tagList = new ArrayList<Tag>();
	}
	
	//ArrayList<String> tags;
	

	public Image getImage() {
		return photoImage;
	}
	
	public String getPath() {
		return photoPath;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String s) {
		this.name = s;
	}
	
	public String getCaption() {
		return this.caption;
	}
	
	public void setCaption(String s) {
		this.caption = s;
	}
	
	public void setDate(String s) {
		this.date = s;
	}
	
	public String getDate() {
		
		return this.date; 
	}
	
	
    public void addTag(String type, String value) throws ClassNotFoundException, IOException {
		//Entry e = new Entry();
    	
    	tagList.add(new Tag(type,value));
		//UserList.getCurrentUser().getCurrentAlbum().photoList.add(photo);
		//UserList.getCurrentUser().getCurrentAlbum().nameList.add(photo.getName());
		//System.out.println("ADDED TO LIST SIZE: " + nameList.size());
		UserList.saveToDAT();
	}
	
	
	
}