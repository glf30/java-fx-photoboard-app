package photos.app;


import java.io.IOException;

/**
 * Jean Moscoso
 * Greyson Frazier
 * 
 */


import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1272880243458262535L;
	private String name;
	public ArrayList<PhotoAlbum> albumList;
	public ArrayList<String> nameList;
	private PhotoAlbum currentAlbum = new PhotoAlbum();
	//private static UserList userlist = new UserList();
	
	public User(String name) 
	{
		albumList = new ArrayList<PhotoAlbum>();
		nameList = new ArrayList<String>();
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public ArrayList<PhotoAlbum> getPhotoAlbum() throws ClassNotFoundException, IOException{
		UserList.getFromDAT();
		
		//System.out.println(UserList.getCurrentUser().albumList.get(0));
		//saveToDAT();
		return albumList;
		
	}
	
	public void addAlbum(PhotoAlbum album) throws ClassNotFoundException, IOException {
		//UserList.getFromDAT();
		
		//System.out.println("successful add: " + album.getName());
		//User.albumList.add(album);
		UserList.getCurrentUser().albumList.add(album);
		UserList.getCurrentUser().nameList.add(album.getName());
		/////UserList.getCurrentUser().albumList.add(album);
		UserList.saveToDAT();
		
	}
	
	public void setCurrentAlbum(PhotoAlbum album) {
		currentAlbum = album;
	}
	
	public PhotoAlbum getCurrentAlbum() {
		return currentAlbum;
	}
	
	public ArrayList<String> getAlbumListAsString()
	{
		UserList.getFromDAT();
		ArrayList<String> list = new ArrayList<>();
		for(PhotoAlbum photoAlbum: albumList)
		{
			list.add(photoAlbum.getName());
		}
		return list;
	}
	
}
