package photos.app;

import java.io.Serializable;


public class Tag implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6619680224678279158L;
	//private static final long serialVersionUID = 9;
	
	
	private String type;
	private String value;
	
	
	
	public Tag(String type, String value) {
		this.type = type;
		this.value = value;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}