package photos.app;

import javafx.application.Application;
import javafx.stage.Stage;
import photos.controller.SwitchController;


/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 */

public class Photos extends Application {
	
	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
	
		SwitchController sceneSwitch = new SwitchController(primaryStage);
		sceneSwitch.setPrimaryStage();
		
	}

	/** Starts the program
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}