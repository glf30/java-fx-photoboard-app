package photos.app;

import java.io.IOException;

/**
 * Jean Moscoso
 * Greyson Frazier
 * 
 */


import java.io.Serializable;
import java.util.ArrayList;
import javafx.scene.image.ImageView;

public class PhotoAlbum implements Serializable{

	/**
	 * 
	 */
	private String name;

	
	
	private ImageView albumImage;
	public ArrayList<Photo> photoList;
	public ArrayList<String> nameList;
	
	private Photo currentPhoto = new Photo("");
	
	public PhotoAlbum () {
		photoList = new ArrayList<Photo>();
		nameList = new ArrayList<String>();		
	}
	
	
	public void setName(String albumTitle) {
		name = albumTitle;
	}
	
	public String getName() {
		return this.name;
	}
	public void changeName(String s) {
		name = s;
		
	}
	
	public void addPhoto(Photo photo) throws ClassNotFoundException, IOException {
		
		UserList.getCurrentUser().getCurrentAlbum().photoList.add(photo);
		UserList.getCurrentUser().getCurrentAlbum().nameList.add(photo.getName());
		UserList.saveToDAT();
	}
	
	public void addPhotoToAlbum(Photo photo, PhotoAlbum photoAlbum) throws ClassNotFoundException, IOException {
		
		photoAlbum.photoList.add(photo);
		photoAlbum.nameList.add(photo.getName());
		UserList.saveToDAT();
	}
	
	public ArrayList<Photo> getPhotoList() throws ClassNotFoundException, IOException{
		UserList.getFromDAT();
		
		//System.out.println(UserList.getCurrentUser().albumList.get(0));
		//saveToDAT();
		return photoList;
		
	}
	
	public void setCurrentImage(Photo photo)
	{
		currentPhoto = photo;
	}
	
	public Photo getCurrentImage()
	{
		return currentPhoto;
	}
	
	public ImageView getAlbumImage() {
		return this.albumImage;
	}
	
	
	
	private static final long serialVersionUID = 4944925890542055499L;

}
