package photos.app;

/**
 * Jean Moscoso
 * Greyson Frazier
 * 
 */


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class UserList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1758992754572134137L;
	protected static Set<User> listOfUsers = new HashSet<>();
	private static User currentUser = new User("");
	
	 @SuppressWarnings({ "unchecked" })
		public static void getFromDAT() {
	    	try
	        {
	            FileInputStream fis = new FileInputStream("user.dat");
	            ObjectInputStream ois = new ObjectInputStream(fis);
	            listOfUsers = (Set<User>) ois.readObject();
	            ois.close();
	            fis.close();
	         }catch(IOException ioe){
	             ioe.printStackTrace();
	             return;
	          }catch(ClassNotFoundException c){
	             System.out.println("Class not found");
	             c.printStackTrace();
	             return;
	          }
		}
	    
	    public static void saveToDAT()
	    {
	    	try{
	            FileOutputStream fos= new FileOutputStream("user.dat");
	            ObjectOutputStream oos= new ObjectOutputStream(fos);
	            oos.writeObject(listOfUsers);
	            //System.out.println("Saved");
	            oos.close();
	            fos.close();
	          }catch(IOException ioe){
	               ioe.printStackTrace();
	           }
	    }

	    
	    
		public static Set<String> getList() {
			Set<String> stringListOfUsers = new TreeSet<>();
			getFromDAT();
			for(User user: listOfUsers)
			{
				stringListOfUsers.add(user.getName());
			}
			return stringListOfUsers;
		}

		public static void add(String name) {
			getFromDAT();
			listOfUsers.add(new User(name));
			saveToDAT();
		}

		public static void remove(String name) {
			getFromDAT();
			for (Iterator<User> iterator = listOfUsers.iterator(); iterator.hasNext();)
			{
				User user = iterator.next();
				if(user.getName().equals(name))
				{
					iterator.remove();
				}
			}
			saveToDAT();
		}
		
		public static void setCurrentUser(String name)
		{
			 for (Iterator<User> iterator = listOfUsers.iterator(); iterator.hasNext();){
	                User user = iterator.next();
	                if(user.getName().equals(name)){
	                	currentUser = user;
	                    break;
	                }
			 }

	    }
	    public static User getCurrentUser() 
	    {
	            return currentUser;
	    }
		
		public static User get(String name)
		{
			getFromDAT();
			for (Iterator<User> iterator = listOfUsers.iterator(); iterator.hasNext();)
			{
				User user = iterator.next();
				if(user.getName().equals(name))
				{
					return user;
				}
			}
			return null;
		}
}
